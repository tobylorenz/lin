# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.1] - 2019-01-10
### Added
- Location Tracking
### Changed
- Parser optimized for non-interactive streams. More windows compatibility.

## [1.0.0] - 2019-01-09
### Added
- Initial version
