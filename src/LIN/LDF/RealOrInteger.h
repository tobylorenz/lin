/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <ostream>
#include <string>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** real or integer (real_or_integer) */
class LIN_LDF_EXPORT RealOrInteger
{
public:
    RealOrInteger();
    RealOrInteger(uint8_t type, std::string & value);

    // 0 = undefined
    // 1 = real
    // 2 = integer
    uint8_t type;

    /** value */
    std::string value;
};

std::ostream & operator<<(std::ostream & os, RealOrInteger & obj);

}
}
