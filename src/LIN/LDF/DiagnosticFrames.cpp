/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/DiagnosticFrames.h>

namespace LIN {
namespace LDF {

DiagnosticFrames::DiagnosticFrames() :
    masterReqId(),
    masterReqFrames(),
    slaveRespId(),
    slaveReqFrames()
{
}

DiagnosticFrames::DiagnosticFrames(std::string & masterReqId, std::map<std::string, DiagnosticFrame> & masterReqFrames, std::string & slaveRespId, std::map<std::string, DiagnosticFrame> & slaveReqFrames) :
    masterReqId(masterReqId),
    masterReqFrames(masterReqFrames),
    slaveRespId(slaveRespId),
    slaveReqFrames(slaveReqFrames)
{
}

std::ostream & operator<<(std::ostream & os, DiagnosticFrames & obj)
{
    os << "Diagnostic_frames {" << std::endl;

    os << "  MasterReq: " << obj.masterReqId << " {" << std::endl;

    for (std::pair<std::string, DiagnosticFrame> diagnosticFrame : obj.masterReqFrames) {
        os << diagnosticFrame.second;
    }

    os << "  }" << std::endl;

    os << "  SlaveResp: " << obj.slaveRespId << " {" << std::endl;

    for (std::pair<std::string, DiagnosticFrame> diagnosticFrame : obj.slaveReqFrames) {
        os << diagnosticFrame.second;
    }

    os << "  }" << std::endl;

    os << "}" << std::endl;

    return os;
}

}
}
