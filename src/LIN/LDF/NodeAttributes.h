/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <ostream>
#include <string>
#include <vector>

#include <LIN/LDF/NodeAttribute.h>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** node attributes (node_attributes_def) */
class LIN_LDF_EXPORT NodeAttributes
{
public:
    NodeAttributes();
    NodeAttributes(std::string & name, std::vector<NodeAttribute> & attributes);

    /** node name (node_name) */
    std::string name;

    /** attributes */
    std::vector<NodeAttribute> attributes;
};

std::ostream & operator<<(std::ostream & os, NodeAttributes & obj);

}
}
