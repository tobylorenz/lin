/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/DiagnosticSignal.h>

namespace LIN {
namespace LDF {

DiagnosticSignal::DiagnosticSignal() :
    name(),
    size(),
    initValue()
{
}

DiagnosticSignal::DiagnosticSignal(std::string & name, std::string & size, InitValue & initValue) :
    name(name),
    size(size),
    initValue(initValue)
{
}

std::ostream & operator<<(std::ostream & os, DiagnosticSignal & obj)
{
    os << "  " << obj.name << ": " << obj.size << ", " << obj.initValue << ";" << std::endl;

    return os;
}

}
}
