/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/InitValue.h>

namespace LIN {
namespace LDF {

InitValue::InitValue() :
    type(0),
    scalar(),
    array()
{
}

InitValue::InitValue(uint8_t type, std::string & scalar) :
    type(type),
    scalar(scalar),
    array()
{
}

InitValue::InitValue(uint8_t type, std::vector<std::string> & array) :
    type(type),
    scalar(),
    array(array)
{
}

std::ostream & operator<<(std::ostream & os, InitValue & obj)
{
    switch (obj.type) {
    case 1:
        os << obj.scalar;
        break;
    case 2: {
        os << "{ ";
        bool first = true;
        for (std::string & str : obj.array) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << str;
        }
        os << " }";
    }
        break;
    }

    return os;
}

}
}
