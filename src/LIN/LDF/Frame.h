/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <map>
#include <ostream>

#include <LIN/LDF/NameOffset.h>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** unconditional frame (frame_def) */
class LIN_LDF_EXPORT Frame
{
public:
    Frame();
    Frame(std::string & name, std::string & id, std::string & publishedBy, std::string & size, std::map<std::string, NameOffset> & nameOffsets);

    /** name (frame_name) */
    std::string name;

    /** id (frame_id) */
    std::string id;

    /** published by (published_by) */
    std::string publishedBy;

    /** frame size (frame_size) */
    std::string size;

    /** signal name offsets (signal_name, signal_offset) */
    std::map<std::string, NameOffset> nameOffsets;
};

std::ostream & operator<<(std::ostream & os, Frame & obj);

}
}
