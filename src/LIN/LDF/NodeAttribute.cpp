/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/NodeAttribute.h>

namespace LIN {
namespace LDF {

NodeAttribute::NodeAttribute() :
    type(0),
    str(),
    supplierId(),
    functionId(),
    variantId(),
    faultStateSignalNames(),
    duration(),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, std::string & str) :
    type(type),
    str(str),
    supplierId(),
    functionId(),
    variantId(),
    faultStateSignalNames(),
    duration(),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, std::string & supplierId, std::string & functionId) :
    type(type),
    str(),
    supplierId(supplierId),
    functionId(functionId),
    variantId(),
    faultStateSignalNames(),
    duration(),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, std::string & supplierId, std::string & functionId, std::string & variantId) :
    type(type),
    str(),
    supplierId(supplierId),
    functionId(functionId),
    variantId(variantId),
    faultStateSignalNames(),
    duration(),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, std::vector<std::string> & faultStateSignals) :
    type(type),
    str(),
    supplierId(),
    functionId(),
    variantId(),
    faultStateSignalNames(faultStateSignals),
    duration(),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, RealOrInteger & duration) :
    type(type),
    str(),
    supplierId(),
    functionId(),
    variantId(),
    faultStateSignalNames(),
    duration(duration),
    configurableFrames()
{
}

NodeAttribute::NodeAttribute(uint8_t type, std::map<std::string, ConfigurableFrame> & configurableFrames) :
    type(type),
    str(),
    supplierId(),
    functionId(),
    variantId(),
    faultStateSignalNames(),
    duration(),
    configurableFrames(configurableFrames)
{
}

std::ostream & operator<<(std::ostream & os, NodeAttribute & obj)
{
    switch (obj.type) {
    case 1:
        os << "    LIN_protocol = " << obj.str << ";" << std::endl;
        break;
    case 2:
        os << "    configured_NAD = " << obj.str << ";" << std::endl;
        break;
    case 3:
        os << "    initial_NAD = " << obj.str << ";" << std::endl;
        break;
    case 4:
        os << "    product_id = " << obj.supplierId << ", " << obj.functionId;
        if (!obj.variantId.empty()) {
            os << ", " << obj.variantId;
        }
        os << ";" << std::endl;
        break;
    case 5:
        os << "    response_error = " << obj.str << ";" << std::endl;
        break;
    case 6: {
        os << "    fault_state_signals = ";
        bool first = true;
        for (std::string & faultStateSignalName : obj.faultStateSignalNames) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << faultStateSignalName;
        }
        os << ";" << std::endl;
    }
        break;
    case 7:
        os << "    P2_min = " << obj.duration << " ms;" << std::endl;
        break;
    case 8:
        os << "    ST_min = " << obj.duration << " ms;" << std::endl;
        break;
    case 9:
        os << "    N_As_timeout = " << obj.duration << " ms;" << std::endl;
        break;
    case 10:
        os << "    N_Cr_timeout = " << obj.duration << " ms;" << std::endl;
        break;
    case 11:
        os << "    configurable_frames {" << std::endl;

        for (std::pair<std::string, ConfigurableFrame> configurableFrame : obj.configurableFrames) {
            os << configurableFrame.second;
        }

        os << "    }" << std::endl;
        break;
    }

    return os;
}

}
}
