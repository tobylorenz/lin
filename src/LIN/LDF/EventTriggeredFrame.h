/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <ostream>
#include <string>
#include <vector>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** event triggered frame (event_triggered_frame_def) */
class LIN_LDF_EXPORT EventTriggeredFrame
{
public:
    EventTriggeredFrame();
    EventTriggeredFrame(std::string & name, std::string & collisionResolvingScheduleTable, std::string & id, std::vector<std::string> & frameNames);

    /** name (event_trig_frm_name) */
    std::string name;

    /** collision resolving schedule table (collision_resolving_schedule_table) */
    std::string collisionResolvingScheduleTable;

    /** id (frame_id) */
    std::string id;

    /** frame names (frame_name) */
    std::vector<std::string> frameNames;
};

std::ostream & operator<<(std::ostream & os, EventTriggeredFrame & obj);

}
}
