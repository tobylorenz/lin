%{
#include <LIN/LDF/LinDescriptionFile.h>
#include <LIN/LDF/Scanner.h>

/* match end of file */
// #define yyterminate() return LIN::LDF::Parser::make_END
%}

    // debug options
    // %option backup
%option debug
%option perf-report
    // %option trace
    // %option verbose

    // standard options
%option nodefault
%option nounput
%option noinput
%option batch
%option yyclass="LIN::LDF::Scanner"
%option noyywrap
%option c++

DIGIT                   [0-9]
NONDIGIT                [_a-zA-Z]
HEXADECIMAL_PREFIX      (0x|0X)
NONZERO_DIGIT           [1-9]
HEXADECIMAL_DIGIT       [0-9a-fA-F]

%x COMMENT

%%

    /* 9.2 LIN description file definition */
"LIN_description_file" {
    return LIN::LDF::Parser::make_LIN_DESCRIPTION_FILE(loc); }

    /* 9.2.1.1 LIN protocol version number definition */
"LIN_protocol_version" {
    return LIN::LDF::Parser::make_LIN_PROTOCOL_VERSION(loc); }

    /* 9.2.1.2 LIN language version number definition */
"LIN_language_version" {
    return LIN::LDF::Parser::make_LIN_LANGUAGE_VERSION(loc); }

    /* 9.2.1.3 LIN speed definition */
"LIN_speed" {
    return LIN::LDF::Parser::make_LIN_SPEED(loc); }
"kbps" {
    return LIN::LDF::Parser::make_KBPS(loc); }

    /* 9.2.1.4 Channel postfix name definition */
"Channel_name" {
    return LIN::LDF::Parser::make_CHANNEL_NAME(loc); }

    /* 9.2.2.1 Participating nodes */
"Nodes" {
    return LIN::LDF::Parser::make_NODES(loc); }
"Master" {
    return LIN::LDF::Parser::make_MASTER(loc); }
"Slaves" {
    return LIN::LDF::Parser::make_SLAVES(loc); }
"ms" {
    return LIN::LDF::Parser::make_MS(loc); }

    /* 9.2.2.2 Node attributes */
"Node_attributes" {
    return LIN::LDF::Parser::make_NODE_ATTRIBUTES(loc); }
"LIN_protocol" {
    return LIN::LDF::Parser::make_LIN_PROTOCOL(loc); }
"configured_NAD" {
    return LIN::LDF::Parser::make_CONFIGURED_NAD(loc); }
"initial_NAD" {
    return LIN::LDF::Parser::make_INITIAL_NAD(loc); }
"product_id" {
    return LIN::LDF::Parser::make_PRODUCT_ID(loc); }
"response_error" {
    return LIN::LDF::Parser::make_RESPONSE_ERROR(loc); }
"fault_state_signals" {
    return LIN::LDF::Parser::make_FAULT_STATE_SIGNALS(loc); }
"P2_min" {
    return LIN::LDF::Parser::make_P2_MIN(loc); }
"ST_min" {
    return LIN::LDF::Parser::make_ST_MIN(loc); }
"N_As_timeout" {
    return LIN::LDF::Parser::make_N_AS_TIMEOUT(loc); }
"N_Cr_timeout" {
    return LIN::LDF::Parser::make_N_CR_TIMEOUT(loc); }
"configurable_frames" {
    return LIN::LDF::Parser::make_CONFIGURABLE_FRAMES(loc); }

    /* 9.2.2.3 Node composition definition */
"composite" {
    return LIN::LDF::Parser::make_COMPOSITE(loc); }
"configuration" {
    return LIN::LDF::Parser::make_CONFIGURATION(loc); }

    /* 9.2.3.1 Standard signals */
"Signals" {
    return LIN::LDF::Parser::make_SIGNALS(loc); }

    /* 9.2.3.2 Diagnostic signals */
"Diagnostic_signals" {
    return LIN::LDF::Parser::make_DIAGNOSTIC_SIGNALS(loc); }

    /* 9.2.3.3 Signal groups */
"Signal_groups" {
    return LIN::LDF::Parser::make_SIGNAL_GROUPS(loc); }

    /* 9.2.4.1 Unconditional frames */
"Frames" {
    return LIN::LDF::Parser::make_FRAMES(loc); }

    /* 9.2.4.2 Sporadic frames */
"Sporadic_frames" {
    return LIN::LDF::Parser::make_SPORADIC_FRAMES(loc); }

    /* 9.2.4.3 Event triggered frames */
"Event_triggered_frames" {
    return LIN::LDF::Parser::make_EVENT_TRIGGERED_FRAMES(loc); }

    /* 9.2.4.4 Diagnostic frames */
"Diagnostic_frames" {
    return LIN::LDF::Parser::make_DIAGNOSTIC_FRAMES(loc); }

    /* 9.2.5 Schedule table definition */
"Schedule_tables" {
    return LIN::LDF::Parser::make_SCHEDULE_TABLES(loc); }
"delay" {
    return LIN::LDF::Parser::make_DELAY(loc); }
"MasterReq" {
    return LIN::LDF::Parser::make_MASTERREQ(loc); }
"SlaveResp" {
    return LIN::LDF::Parser::make_SLAVERESP(loc); }
"AssignNAD" {
    return LIN::LDF::Parser::make_ASSIGNNAD(loc); }
"ConditionalChangeNAD" {
    return LIN::LDF::Parser::make_CONDITIONALCHANGENAD(loc); }
"DataDump" {
    return LIN::LDF::Parser::make_DATADUMP(loc); }
"SaveConfiguration" {
    return LIN::LDF::Parser::make_SAVECONFIGURATION(loc); }
"AssignFrameIdRange" {
    return LIN::LDF::Parser::make_ASSIGNFRAMEIDRANGE(loc); }
"FreeFormat" {
    return LIN::LDF::Parser::make_FREEFORMAT(loc); }
"AssignFrameId" {
    return LIN::LDF::Parser::make_ASSIGNFRAMEID(loc); }

    /* 9.2.6.1 Signal encoding type definition */
"Signal_encoding_types" {
    return LIN::LDF::Parser::make_SIGNAL_ENCODING_TYPES(loc); }
"logical_value" {
    return LIN::LDF::Parser::make_LOGICAL_VALUE(loc); }
"physical_value" {
    return LIN::LDF::Parser::make_PHYSICAL_VALUE(loc); }
"bcd_value" {
    return LIN::LDF::Parser::make_BCD_VALUE(loc); }
"ascii_value" {
    return LIN::LDF::Parser::make_ASCII_VALUE(loc); }

    /* 9.2.6.2 Signal representation definition */
"Signal_representation" {
    return LIN::LDF::Parser::make_SIGNAL_REPRESENTATION(loc); }

    /* 9.3 Overview of syntax */
\"(\\.|[^\\"\n])*\" {
    return LIN::LDF::Parser::make_CHAR_STRING(yytext, loc); }
{NONDIGIT}({NONDIGIT}|{DIGIT})* {
    return LIN::LDF::Parser::make_IDENTIFIER(yytext, loc); }
{DIGIT}+ {
    return LIN::LDF::Parser::make_INTEGER(yytext, loc); }
{HEXADECIMAL_PREFIX}{HEXADECIMAL_DIGIT}+ {
    return LIN::LDF::Parser::make_INTEGER(yytext, loc); }
{DIGIT}*"."{DIGIT}+ {
    return LIN::LDF::Parser::make_REAL(yytext, loc); }

    /* Punctuators */
"{" {
    return LIN::LDF::Parser::make_OPEN_BRACE(loc); }
"}" {
    return LIN::LDF::Parser::make_CLOSE_BRACE(loc); }
":" {
    return LIN::LDF::Parser::make_COLON(loc); }
";" {
    return LIN::LDF::Parser::make_SEMICOLON(loc); }
"=" {
    return LIN::LDF::Parser::make_ASSIGN(loc); }
"," {
    return LIN::LDF::Parser::make_COMMA(loc); }

    /* Comments */
"/*" {
    BEGIN(COMMENT); }
<COMMENT>([^*]|\n)+|. {
    }
<COMMENT>"*/" {
    BEGIN(INITIAL); }
"//".*\n {
    }

    /* whitespace */
[ \t\v\n\f\r] {
    }

    /* yet unmatched characters */
. {
    std::cerr << "unmatched character: " << *yytext; }

    /* match end of file */
<<EOF>> {
    return LIN::LDF::Parser::make_END(loc); }

%%

