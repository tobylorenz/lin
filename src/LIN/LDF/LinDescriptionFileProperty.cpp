/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/LinDescriptionFileProperty.h>

namespace LIN {
namespace LDF {

LinDescriptionFileProperty::LinDescriptionFileProperty() :
    type(0),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::string & str) :
    type(type),
    str(str),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, RealOrInteger & speed) :
    type(type),
    str(),
    speed(speed),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, Nodes & node) :
    type(type),
    str(),
    speed(),
    node(node),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, NodeConfiguration> & nodeConfigurations) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(nodeConfigurations),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, Signal> & signals) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(signals),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, DiagnosticSignal> & diagnosticSignals) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(diagnosticSignals),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, Frame> & frames) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(frames),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, SporadicFrame> & sporadicFrames) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(sporadicFrames),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, EventTriggeredFrame> & eventTriggeredFrames) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(eventTriggeredFrames),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, DiagnosticFrames & diagnosticFrames) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(diagnosticFrames),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, NodeAttributes> & nodeAttributes) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(nodeAttributes),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, ScheduleTable> & scheduleTables) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(scheduleTables),
    signalGroups(),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, SignalGroup> & signalGroups) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(signalGroups),
    encodings(),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, Encoding> & encodings) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(encodings),
    signalRepresentations()
{
}

LinDescriptionFileProperty::LinDescriptionFileProperty(uint8_t type, std::map<std::string, SignalRepresentation> & signalRepresentations) :
    type(type),
    str(),
    speed(),
    node(),
    nodeConfigurations(),
    signals(),
    diagnosticSignals(),
    frames(),
    sporadicFrames(),
    eventTriggeredFrames(),
    diagnosticFrames(),
    nodeAttributes(),
    scheduleTables(),
    signalGroups(),
    encodings(),
    signalRepresentations(signalRepresentations)
{
}

std::ostream & operator<<(std::ostream & os, LinDescriptionFileProperty & obj)
{
    switch (obj.type) {
    case 1:
        os << "LIN_protocol_version = " << obj.str << ";" << std::endl;
        break;
    case 2:
        os << "LIN_language_version = " << obj.str << ";" << std::endl;
        break;
    case 3:
        os << "LIN_speed = " << obj.speed << " kbps;" << std::endl;
        break;
    case 4:
        os << "Channel_name = " << obj.str << ";" << std::endl;
        break;
    case 5:
        os << obj.node;
        break;
    case 6:
        os << "composite {" << std::endl;

        for (std::pair<std::string, NodeConfiguration> nodeConfiguration : obj.nodeConfigurations) {
            os << nodeConfiguration.second;
        }

        os << "}" << std::endl;
        break;
    case 7:
        os << "Signals {" << std::endl;

        for (std::pair<std::string, Signal> signal : obj.signals) {
            os << signal.second;
        }

        os << "}" << std::endl;
        break;
    case 8:
        os << "Diagnostic_signals {" << std::endl;

        for (std::pair<std::string, DiagnosticSignal> diagnosticSignal : obj.diagnosticSignals) {
            os << diagnosticSignal.second;
        }

        os << "}" << std::endl;
        break;
    case 9:
        os << "Frames {" << std::endl;

        for (std::pair<std::string, Frame> frame : obj.frames) {
            os << frame.second;
        }

        os << "}" << std::endl;
        break;
    case 10:
        os << "Sporadic_frames {" << std::endl;

        for (std::pair<std::string, SporadicFrame> sporadicFrame : obj.sporadicFrames) {
            os << sporadicFrame.second;
        }

        os << "}" << std::endl;
        break;
    case 11:
        os << "Event_triggered_frames {" << std::endl;

        for (std::pair<std::string, EventTriggeredFrame> eventTriggeredFrame : obj.eventTriggeredFrames) {
            os << eventTriggeredFrame.second;
        }

        os << "}" << std::endl;
        break;
    case 12:
        os << obj.diagnosticFrames;
        break;
    case 13:
        os << "Node_attributes {" << std::endl;

        for (std::pair<std::string, NodeAttributes> nodeAttribute : obj.nodeAttributes) {
            os << nodeAttribute.second;
        }

        os << "}" << std::endl;
        break;
    case 14:
        os << "Schedule_tables {" << std::endl;

        for (std::pair<std::string, ScheduleTable> scheduleTable : obj.scheduleTables) {
            os << scheduleTable.second;
        }

        os << "}" << std::endl;
        break;
    case 15:
        os << "Signal_groups {" << std::endl;

        for (std::pair<std::string, SignalGroup> signalGroup : obj.signalGroups) {
            os << signalGroup.second;
        }

        os << "}" << std::endl;
        break;
    case 16:
        os << "Signal_encoding_types {" << std::endl;

        for (std::pair<std::string, Encoding> encoding : obj.encodings) {
            os << encoding.second;
        }

        os << "}" << std::endl;
        break;
    case 17:
        os << "Signal_representation {" << std::endl;

        for (std::pair<std::string, SignalRepresentation> signalRepresentation : obj.signalRepresentations) {
            os << signalRepresentation.second;
        }

        os << "}" << std::endl;
        break;
    }

    return os;
}

}
}
