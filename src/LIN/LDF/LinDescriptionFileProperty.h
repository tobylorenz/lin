/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <cstdint>
#include <ostream>
#include <map>
#include <string>
#include <vector>

#include <LIN/LDF/DiagnosticFrames.h>
#include <LIN/LDF/DiagnosticSignal.h>
#include <LIN/LDF/EventTriggeredFrame.h>
#include <LIN/LDF/Encoding.h>
#include <LIN/LDF/Frame.h>
#include <LIN/LDF/NodeAttributes.h>
#include <LIN/LDF/NodeConfiguration.h>
#include <LIN/LDF/Nodes.h>
#include <LIN/LDF/RealOrInteger.h>
#include <LIN/LDF/ScheduleTable.h>
#include <LIN/LDF/Signal.h>
#include <LIN/LDF/SignalGroup.h>
#include <LIN/LDF/SignalRepresentation.h>
#include <LIN/LDF/SporadicFrame.h>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** LIN description file properties */
class LIN_LDF_EXPORT LinDescriptionFileProperty
{
public:
    LinDescriptionFileProperty();
    LinDescriptionFileProperty(uint8_t type, std::string & str);
    LinDescriptionFileProperty(uint8_t type, RealOrInteger & speed);
    LinDescriptionFileProperty(uint8_t type, Nodes & node);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, NodeConfiguration> & nodeConfigurations);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, Signal> & signals);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, DiagnosticSignal> & diagnosticSignals);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, Frame> & frames);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, SporadicFrame> & sporadicFrames);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, EventTriggeredFrame> & eventTriggeredFrames);
    LinDescriptionFileProperty(uint8_t type, DiagnosticFrames & diagnosticFrames);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, NodeAttributes> & nodeAttributes);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, ScheduleTable> & scheduleTables);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, SignalGroup> & signalGroups);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, Encoding> & encodings);
    LinDescriptionFileProperty(uint8_t type, std::map<std::string, SignalRepresentation> & signalRepresentations);

    // 0 = undefined
    // 1 = LIN_protocol_version_def
    // 2 = LIN_language_version_def
    // 3 = LIN_speed_def
    // 4 = Channel_name_def
    // 5 = Node_def
    // 6 = Node_composition_def
    // 7 = Signal_def
    // 8 = Diag_signal_def
    // 9 = Frame_def
    // 10 = Sporadic_frame_def
    // 11 = Event_triggered_frame_def
    // 12 = Diag_frame_def
    // 13 = Node_attributes_def
    // 14 = Schedule_table_def
    // 15 = Signal_groups_def
    // 16 = Signal_encoding_type_def
    // 17 = Signal_representation_def
    uint8_t type;

    /**
     * protocol version number (LIN_protocol_version_def)
     * language version number (LIN_language_version_def)
     * channel postfix name (Channel_name_def)
     */
    std::string str;

    /** speed (LIN_speed_def) */
    RealOrInteger speed;

    /** nodes (Node_def) */
    Nodes node;

    /** node configurations (Node_composition_def) */
    std::map<std::string, NodeConfiguration> nodeConfigurations;

    /** signals (Signal_def) */
    std::map<std::string, Signal> signals;

    /** diagnostic signals (Diag_signal_def) */
    std::map<std::string, DiagnosticSignal> diagnosticSignals;

    /** unconditional frames (Frame_def) */
    std::map<std::string, Frame> frames;

    /** sporadic frames (Sporadic_frame_def) */
    std::map<std::string, SporadicFrame> sporadicFrames;

    /** event triggered frames (Event_triggered_frame_def) */
    std::map<std::string, EventTriggeredFrame> eventTriggeredFrames;

    /** diagnostic frames (Diag_frame_def) */
    DiagnosticFrames diagnosticFrames;

    /** node attributes (Node_attributes_def) */
    std::map<std::string, NodeAttributes> nodeAttributes;

    /** schedule tables (Schedule_table_def) */
    std::map<std::string, ScheduleTable> scheduleTables;

    /** signal groups (Signal_groups_def) */
    std::map<std::string, SignalGroup> signalGroups;

    /** signal encoding types (Signal_encoding_type_def) */
    std::map<std::string, Encoding> encodings;

    /** signal representations (Signal_representation_def) */
    std::map<std::string, SignalRepresentation> signalRepresentations;
};

std::ostream & operator<<(std::ostream & os, LinDescriptionFileProperty & obj);

}
}
