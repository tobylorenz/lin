/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#define BOOST_TEST_MODULE LinDescriptionFile
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <fstream>
#include <boost/filesystem.hpp>

#include <LIN/LDF/LinDescriptionFile.h>

BOOST_AUTO_TEST_CASE(LinDescriptionFile)
{
    /* read file */
    boost::filesystem::path infile(CMAKE_CURRENT_SOURCE_DIR "/data/test.ldf");
    std::fstream ifs;
    ifs.open(infile.c_str(), std::ios_base::in);
    BOOST_REQUIRE(ifs.is_open());
    LIN::LDF::LinDescriptionFile linDescriptionFile;
    ifs >> linDescriptionFile;
    ifs.close();
    BOOST_CHECK(linDescriptionFile.successfullyParsed);

    /* create output directory */
    boost::filesystem::path outdir(CMAKE_CURRENT_BINARY_DIR "/data/");
    if (!exists(outdir)) {
        BOOST_REQUIRE(create_directory(outdir));
    }

    /* write file */
    boost::filesystem::path outfile(CMAKE_CURRENT_BINARY_DIR "/data/test.ldf");
    std::fstream ofs;
    ofs.open(outfile.c_str(), std::ios_base::out);
    BOOST_REQUIRE(ofs.is_open());
    ofs << linDescriptionFile;
    ofs.close();

    /* loaded and saved file should be equivalent */
    std::ifstream ifs1(infile.c_str());
    std::ifstream ifs2(outfile.c_str());
    std::istream_iterator<char> b1(ifs1), e1;
    std::istream_iterator<char> b2(ifs2), e2;
    BOOST_CHECK_EQUAL_COLLECTIONS(b1, e1, b2, e2);
}
