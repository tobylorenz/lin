/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/Frame.h>

namespace LIN {
namespace LDF {

Frame::Frame() :
    name(),
    id(),
    publishedBy(),
    size(),
    nameOffsets()
{
}

Frame::Frame(std::string & name, std::string & id, std::string & publishedBy, std::string & size, std::map<std::string, NameOffset> & nameOffsets) :
    name(name),
    id(id),
    publishedBy(publishedBy),
    size(size),
    nameOffsets(nameOffsets)
{
}

std::ostream & operator<<(std::ostream & os, Frame & obj)
{
    os << "  " << obj.name << ": " << obj.id << ", " << obj.publishedBy << ", " << obj.size << " {" << std::endl;

    for (std::pair<std::string, NameOffset> nameOffset : obj.nameOffsets) {
        os << nameOffset.second;
    }

    os << "  }" << std::endl;

    return os;
}

}
}
