/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/LDF/platform.h>

#include <cstdint>
#include <ostream>
#include <string>
#include <vector>

#include <LIN/LDF/lin_ldf_export.h>

namespace LIN {
namespace LDF {

/** command (command) */
class LIN_LDF_EXPORT Command
{
public:
    Command();
    Command(uint8_t type);
    Command(uint8_t type, std::string & str1);
    Command(uint8_t type, std::string & nad, std::string & id, std::string & byte, std::string & mask, std::string & inv, std::string & newNad);
    Command(uint8_t type, std::string & str1, std::vector<std::string> data);
    Command(uint8_t type, std::string & str1, std::string & str2, std::vector<std::string> & framePid);
    Command(uint8_t type, std::vector<std::string> data);
    Command(uint8_t type, std::string & str1, std::string & str2);

    // 0 = undefined
    // 1 = frame_name
    // 2 = MasterReq
    // 3 = SlaveResp
    // 4 = AssignNAD
    // 5 = ConditionalChangeNAD
    // 6 = DataDump
    // 7 = SaveConfiguration
    // 8 = AssignFrameIdRange
    // 9 = FreeFormat
    // 10 = AssignFrameId
    uint8_t type;

    /**
     * frame name (frame_name)
     * node name (node_name)
     */
    std::string str1;

    /**
     * frame index (frame_index)
     * frame name (frame_name)
     */
    std::string str2;

    /** node address (NAD) */
    std::string nad;

    /** identifier (id) */
    std::string id;

    /** (byte) */
    std::string byte;

    /** (mask) */
    std::string mask;

    /** (inv) */
    std::string inv;

    /** new node address (new_NAD) */
    std::string newNad;

    /** frame PID (frame_PID) */
    std::vector<std::string> framePids;

    /** data (D1..8) */
    std::vector<std::string> data;
};

std::ostream & operator<<(std::ostream & os, Command & obj);

}
}
