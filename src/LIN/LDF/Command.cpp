/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/LDF/Command.h>

namespace LIN {
namespace LDF {

Command::Command() :
    type(0),
    str1(),
    str2(),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data()
{
}

Command::Command(uint8_t type) :
    type(type),
    str1(),
    str2(),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data()
{
}

Command::Command(uint8_t type, std::string & str1) :
    type(type),
    str1(str1),
    str2(),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data()
{
}

Command::Command(uint8_t type, std::string & nad, std::string & id, std::string & byte, std::string & mask, std::string & inv, std::string & newNad) :
    type(type),
    str1(),
    str2(),
    nad(nad),
    id(id),
    byte(byte),
    mask(mask),
    inv(inv),
    newNad(newNad),
    framePids(),
    data()
{
}

Command::Command(uint8_t type, std::string & str1, std::vector<std::string> data) :
    type(type),
    str1(str1),
    str2(),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data(data)
{
}

Command::Command(uint8_t type, std::string & str1, std::string & str2, std::vector<std::string> & framePids) :
    type(type),
    str1(str1),
    str2(str2),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(framePids),
    data()
{
}

Command::Command(uint8_t type, std::vector<std::string> data) :
    type(type),
    str1(),
    str2(),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data(data)
{
}

Command::Command(uint8_t type, std::string & str1, std::string & str2) :
    type(type),
    str1(str1),
    str2(str2),
    nad(),
    id(),
    byte(),
    mask(),
    inv(),
    newNad(),
    framePids(),
    data()
{
}

std::ostream & operator<<(std::ostream & os, Command & obj)
{
    switch (obj.type) {
    case 1:
        os << obj.str1;
        break;
    case 2:
        os << "MasterReq";
        break;
    case 3:
        os << "SlaveResp";
        break;
    case 4:
        os << "AssignNAD { " << obj.str1 << " }";
        break;
    case 5:
        os << "ConditionalChangeNAD { " << obj.nad << ", " << obj.id << ", " << obj.byte << ", " << obj.mask << ", " << obj.inv << ", " << obj.newNad << " }";
        break;
    case 6:
        os << "DataDump { " << obj.str1;
        for (std::string & d : obj.data) {
            os << ", " << d;
        }
        os << " }";
        break;
    case 7:
        os << "SaveConfiguration { " << obj.str1 << " }";
        break;
    case 8:
        os << "AssignFrameIdRange { " << obj.str1 << ", " << obj.str2;
        for (std::string & pid : obj.framePids) {
            os << ", " << pid;
        }
        os << " }";
        break;
    case 9: {
        os << "FreeFormat { ";
        bool first = true;
        for (std::string & d : obj.data) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << d;
        }
        os << " }";
    }
        break;
    case 10:
        os << "AssignFrameId { " << obj.str1 << ", " << obj.str2 << " }";
        break;
    }

    return os;
}

}
}
