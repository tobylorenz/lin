%skeleton "lalr1.cc"
%require "3.0"
%defines
%define api.namespace {LIN::LDF}
%define api.token.constructor
%define api.value.type variant
%define parse.assert
%define parse.trace
%define parser_class_name {Parser}
%language "C++"
%locations

    // debug options
%verbose
%debug
%error-verbose

%code requires{
#include <LIN/LDF/LinDescriptionFileProperty.h>
namespace LIN {
namespace LDF {
class LinDescriptionFile;
}
}
}

%lex-param { const LIN::LDF::Parser::location_type & loc }
%parse-param { class Scanner * scanner }
%parse-param { class LinDescriptionFile * linDescriptionFile }

%code{
#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

#include <LIN/LDF/LinDescriptionFile.h>
#include <LIN/LDF/Scanner.h>

#undef yylex
#define yylex scanner->yylex

#define loc scanner->location
}

    // %destructor { delete($$); ($$) = nullptr; } <lin_description_file>

/* 9.2 LIN description file definition */
%token LIN_DESCRIPTION_FILE
%type <LinDescriptionFileProperty> lin_description_file_property
%type <std::vector<LinDescriptionFileProperty>> lin_description_file_property_list

/* 9.2.1.1 LIN protocol version number definition */
%token LIN_PROTOCOL_VERSION
%type <std::string> lin_protocol_version_def

/* 9.2.1.2 LIN language version number definition */
%token LIN_LANGUAGE_VERSION
%type <std::string> lin_language_version_def

/* 9.2.1.3 LIN speed definition */
%token LIN_SPEED KBPS
%type <RealOrInteger> lin_speed_def

/* 9.2.1.4 Channel postfix name definition */
%token CHANNEL_NAME
%type <std::string> channel_name_def

/* 9.2.2.1 Participating nodes */
%token NODES MASTER SLAVES MS
%type <Nodes> node_def
%type <std::string> node_name
%type <std::vector<std::string>> node_name_list
%type <RealOrInteger> time_base
%type <RealOrInteger> jitter

/* 9.2.2.2 Node attributes */
%token NODE_ATTRIBUTES LIN_PROTOCOL CONFIGURED_NAD INITIAL_NAD PRODUCT_ID RESPONSE_ERROR FAULT_STATE_SIGNALS P2_MIN ST_MIN N_AS_TIMEOUT N_CR_TIMEOUT CONFIGURABLE_FRAMES
%type <std::map<std::string, NodeAttributes>> node_attributes_def
%type <NodeAttributes> single_node_attributes_def
%type <std::map<std::string, NodeAttributes>> single_node_attributes_def_list single_node_attributes_def_list.opt
%type <std::string> protocol_version
%type <std::string> diag_address
%type <NodeAttribute> attribute_def
%type <std::vector<NodeAttribute>> attribute_def_list
%type <std::string> supplier_id
%type <std::string> function_id
%type <std::string> variant_id
%type <std::map<std::string, ConfigurableFrame>> configurable_frames_def
%type <ConfigurableFrame> single_configurable_frame
%type <std::map<std::string, ConfigurableFrame>> single_configurable_frame_list single_configurable_frame_list.opt
%type <std::string> message_id

/* 9.2.2.3 Node composition definition */
%token COMPOSITE CONFIGURATION
%type <std::map<std::string, NodeConfiguration>> node_composition_def
%type <NodeConfiguration> node_configuration_def
%type <std::map<std::string, NodeConfiguration>> node_configuration_def_list node_configuration_def_list.opt
%type <NodeComposition> single_node_composition_def
%type <std::map<std::string, NodeComposition>> single_node_composition_def_list single_node_composition_def_list.opt
%type <std::vector<std::string>> logical_node_list
%type <std::string> configuration_name
%type <std::string> composite_name
%type <std::string> logical_node

/* 9.2.3.1 Standard signals */
%token SIGNALS
%type <std::map<std::string, Signal>> signal_def
%type <Signal> single_signal_def
%type <std::map<std::string, Signal>> single_signal_def_list single_signal_def_list.opt
%type <std::string> signal_name
%type <std::string> signal_size
%type <InitValue> init_value
%type <std::string> init_value_scalar
%type <std::vector<std::string>> init_value_array
%type <std::string> published_by
%type <std::string> subscribed_by
%type <std::vector<std::string>> subscribed_by_list.opt

/* 9.2.3.2 Diagnostic signals */
%token DIAGNOSTIC_SIGNALS
%type <std::map<std::string, DiagnosticSignal>> diagnostic_signal_def
%type <DiagnosticSignal> single_diagnostic_signal_def
%type <std::map<std::string, DiagnosticSignal>> single_diagnostic_signal_def_list single_diagnostic_signal_def_list.opt

/* 9.2.3.3 Signal groups */
%token SIGNAL_GROUPS
%type <std::map<std::string, SignalGroup>> signal_groups_def
%type <SignalGroup> single_signal_group_def
%type <std::map<std::string, SignalGroup>> single_signal_group_def_list single_signal_group_def_list.opt
%type <NameOffset> signal_name_group_offset_def
%type <std::map<std::string, NameOffset>> signal_name_group_offset_def_list signal_name_group_offset_def_list.opt
%type <std::string> signal_group_name
%type <std::string> group_size
%type <std::string> group_offset

/* 9.2.4.1 Unconditional frames */
%token FRAMES
%type <std::map<std::string, Frame>> frame_def
%type <Frame> single_frame_def
%type <std::map<std::string, Frame>> single_frame_def_list single_frame_def_list.opt
%type <NameOffset> signal_name_offset_def
%type <std::map<std::string, NameOffset>> signal_name_offset_def_list signal_name_offset_def_list.opt
%type <std::string> frame_name frame_name.opt
%type <std::string> frame_id
%type <std::string> frame_size
%type <std::string> signal_offset

/* 9.2.4.2 Sporadic frames */
%token SPORADIC_FRAMES
%type <std::map<std::string, SporadicFrame>> sporadic_frame_def
%type <SporadicFrame> single_sporadic_frame_def
%type <std::vector<std::string>> frame_name_list_comma frame_name_list_comma.opt
%type <std::map<std::string, SporadicFrame>> single_sporadic_frame_def_list single_sporadic_frame_def_list.opt
%type <std::string> sporadic_frame_name

/* 9.2.4.3 Event triggered frames */
%token EVENT_TRIGGERED_FRAMES
%type <std::map<std::string, EventTriggeredFrame>> event_triggered_frame_def
%type <EventTriggeredFrame> single_event_triggered_frame_def
%type <std::map<std::string, EventTriggeredFrame>> single_event_triggered_frame_def_list single_event_triggered_frame_def_list.opt
%type <std::string> event_trig_frm_name
%type <std::string> collision_resolving_schedule_table

/* 9.2.4.4 Diagnostic frames */
%token DIAGNOSTIC_FRAMES
%type <DiagnosticFrames> diag_frame_def
%type <DiagnosticFrame> single_diag_frame_def
%type <std::map<std::string, DiagnosticFrame>> single_diag_frame_def_list single_diag_frame_def_list.opt

/* 9.2.5 Schedule table definition */
%token SCHEDULE_TABLES DELAY MASTERREQ SLAVERESP ASSIGNNAD CONDITIONALCHANGENAD DATADUMP SAVECONFIGURATION ASSIGNFRAMEIDRANGE FREEFORMAT ASSIGNFRAMEID
%type <std::map<std::string, ScheduleTable>> schedule_table_def
%type <ScheduleTable> single_schedule_table_def
%type <std::map<std::string, ScheduleTable>> single_schedule_table_def_list single_schedule_table_def_list.opt
%type <CommandTime> single_command_def
%type <std::vector<CommandTime>> single_command_def_list single_command_def_list.opt
%type <std::string> schedule_table_name
%type <Command> command
%type <std::vector<std::string>> frame_pids frame_pids.opt
%type <std::string> frame_index
%type <std::string> frame_pid
%type <RealOrInteger> frame_time

/* 9.2.6.1 Signal encoding type definition */
%token SIGNAL_ENCODING_TYPES LOGICAL_VALUE PHYSICAL_VALUE BCD_VALUE ASCII_VALUE
%type <std::map<std::string, Encoding>> signal_encoding_type_def
%type <Encoding> single_signal_encoding_type_def
%type <std::map<std::string, Encoding>> single_signal_encoding_type_def_list single_signal_encoding_type_def_list.opt
%type <EncodingValue> signal_encoding_value
%type <std::vector<EncodingValue>> signal_encoding_value_list signal_encoding_value_list.opt
%type <std::string> signal_encoding_type_name
%type <LogicalValue> logical_value
%type <PhysicalRange> physical_range
%type <std::string> signal_value
%type <std::string> min_value
%type <std::string> max_value
%type <RealOrInteger> scale
%type <RealOrInteger> offset
%type <std::string> text_info text_info.opt

/* 9.2.6.2 Signal representation definition */
%token SIGNAL_REPRESENTATION
%type <std::map<std::string, SignalRepresentation>> signal_representation_def
%type <SignalRepresentation> single_signal_representation_def
%type <std::map<std::string, SignalRepresentation>> single_signal_representation_def_list single_signal_representation_def_list.opt
%type <std::vector<std::string>> signal_name_list signal_name_list.opt

/* 9.3 Overview of syntax */
%token <std::string> CHAR_STRING IDENTIFIER INTEGER REAL
%type <std::vector<std::string>> integer_list
%type <RealOrInteger> real_or_integer

/* Punctuators */
%token OPEN_BRACE CLOSE_BRACE COLON SEMICOLON ASSIGN COMMA

%start lin_description_file

%token END 0

%%

    /* 9.2 LIN description file definition */
lin_description_file // LIN_description_file in specification
        : LIN_DESCRIPTION_FILE SEMICOLON
          lin_description_file_property_list { linDescriptionFile->properties = $3; }
        ;
lin_description_file_property // not in specification
        : lin_protocol_version_def { $$ = LinDescriptionFileProperty(1, $1); }
        | lin_language_version_def { $$ = LinDescriptionFileProperty(2, $1); }
        | lin_speed_def { $$ = LinDescriptionFileProperty(3, $1); }
        | channel_name_def { $$ = LinDescriptionFileProperty(4, $1); }
        | node_def { $$ = LinDescriptionFileProperty(5, $1); }
        | node_composition_def { $$ = LinDescriptionFileProperty(6, $1); }
        | signal_def { $$ = LinDescriptionFileProperty(7, $1); }
        | diagnostic_signal_def { $$ = LinDescriptionFileProperty(8, $1); }
        | frame_def { $$ = LinDescriptionFileProperty(9, $1); }
        | sporadic_frame_def { $$ = LinDescriptionFileProperty(10, $1); }
        | event_triggered_frame_def { $$ = LinDescriptionFileProperty(11, $1); }
        | diag_frame_def { $$ = LinDescriptionFileProperty(12, $1); }
        | node_attributes_def { $$ = LinDescriptionFileProperty(13, $1); }
        | schedule_table_def { $$ = LinDescriptionFileProperty(14, $1); }
        | signal_groups_def { $$ = LinDescriptionFileProperty(15, $1); }
        | signal_encoding_type_def { $$ = LinDescriptionFileProperty(16, $1); }
        | signal_representation_def { $$ = LinDescriptionFileProperty(17, $1); }
        ;
lin_description_file_property_list // not in specification
        : lin_description_file_property { $$ = std::vector<LinDescriptionFileProperty>(); $$.push_back($1); }
        | lin_description_file_property_list lin_description_file_property { $$ = $1; $$.push_back($2); }
        ;

    /* 9.2.1.1 LIN protocol version number definition */
lin_protocol_version_def // LIN_protocol_version_def in specification
        : LIN_PROTOCOL_VERSION ASSIGN CHAR_STRING SEMICOLON { $$ = $3; }
        ;

    /* 9.2.1.2 LIN language version number definition */
lin_language_version_def // LIN_language_version_def in specification
        : LIN_LANGUAGE_VERSION ASSIGN CHAR_STRING SEMICOLON { $$ = $3; }
        ;

    /* 9.2.1.3 LIN speed definition */
lin_speed_def // LIN_speed_def in specification
        : LIN_SPEED ASSIGN real_or_integer KBPS SEMICOLON { $$ = $3; }
        ;

    /* 9.2.1.4 Channel postfix name definition */
channel_name_def // Channel_name_def in specification
        : CHANNEL_NAME ASSIGN CHAR_STRING SEMICOLON { $$ = $3; }
        ;

    /* 9.2.2.1 Participating nodes */
node_def // Node_def in specification
        : NODES OPEN_BRACE
          MASTER COLON node_name COMMA time_base MS COMMA jitter MS SEMICOLON
          SLAVES COLON node_name_list SEMICOLON
          CLOSE_BRACE { $$ = Nodes($5, $7, $10, $15); }
        ;
node_name // node_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
node_name_list // not in specification
        : node_name { $$ = std::vector<std::string>(); $$.push_back($1); }
        | node_name_list COMMA node_name { $$ = $1; $$.push_back($3); }
        ;
time_base // time_base in specification
        : real_or_integer { $$ = $1; }
        ;
jitter // jitter in specification
        : real_or_integer { $$ = $1; }
        ;

    /* 9.2.2.2 Node attributes */
node_attributes_def // Node_attributes_def in specification
        : NODE_ATTRIBUTES OPEN_BRACE
          single_node_attributes_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_node_attributes_def // not in specification
        : node_name OPEN_BRACE
          attribute_def_list
          CLOSE_BRACE { $$ = NodeAttributes($1, $3); }
        ;
single_node_attributes_def_list // not in specification
        : single_node_attributes_def { $$ = std::map<std::string, NodeAttributes>(); $$[$1.name] = $1; }
        | single_node_attributes_def_list single_node_attributes_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_node_attributes_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, NodeAttributes>(); }
        | single_node_attributes_def_list { $$ = $1; }
        ;
protocol_version // protocol_version in specification
        : CHAR_STRING { $$ = $1; }
        ;
diag_address // diag_address in specification
        : INTEGER { $$ = $1; }
        ;
attribute_def // attributes_def in specification
        : LIN_PROTOCOL ASSIGN protocol_version SEMICOLON { $$ = NodeAttribute(1, $3); }
        | CONFIGURED_NAD ASSIGN diag_address SEMICOLON { $$ = NodeAttribute(2, $3); }
        | INITIAL_NAD ASSIGN diag_address SEMICOLON { $$ = NodeAttribute(3, $3); }
        | PRODUCT_ID ASSIGN supplier_id COMMA function_id SEMICOLON { $$ = NodeAttribute(4, $3, $5); }
        | PRODUCT_ID ASSIGN supplier_id COMMA function_id COMMA variant_id SEMICOLON { $$ = NodeAttribute(4, $3, $5, $7); }
        | RESPONSE_ERROR ASSIGN signal_name SEMICOLON { $$ = NodeAttribute(5, $3); }
        | FAULT_STATE_SIGNALS ASSIGN signal_name_list SEMICOLON { $$ = NodeAttribute(6, $3); }
        | P2_MIN ASSIGN real_or_integer MS SEMICOLON { $$ = NodeAttribute(7, $3); }
        | ST_MIN ASSIGN real_or_integer MS SEMICOLON { $$ = NodeAttribute(8, $3); }
        | N_AS_TIMEOUT ASSIGN real_or_integer MS SEMICOLON { $$ = NodeAttribute(9, $3); }
        | N_CR_TIMEOUT ASSIGN real_or_integer MS SEMICOLON { $$ = NodeAttribute(10, $3); }
        | configurable_frames_def { $$ = NodeAttribute(11, $1); }
        ;
attribute_def_list // not in specification
        : attribute_def { $$ = std::vector<NodeAttribute>(); $$.push_back($1); }
        | attribute_def_list attribute_def { $$ = $1; $$.push_back($2); }
        ;
supplier_id // supplier_id in specification
        : INTEGER { $$ = $1; }
        ;
function_id // function_id in specification
        : INTEGER { $$ = $1; }
        ;
variant_id // variant_id in specification
        : INTEGER { $$ = $1; }
        ;
configurable_frames_def // configurable_frames_20_def/configurable_frames_21_def in specification
        : CONFIGURABLE_FRAMES OPEN_BRACE
          single_configurable_frame_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_configurable_frame // not in specification
        : frame_name ASSIGN message_id SEMICOLON { $$ = ConfigurableFrame(1, $1, $3); } // for 2.0
        | frame_name SEMICOLON { $$ = ConfigurableFrame(2, $1); } // for 2.1
        ;
single_configurable_frame_list // not in specification
        : single_configurable_frame { $$ = std::map<std::string, ConfigurableFrame>(); $$[$1.name] = $1; }
        | single_configurable_frame_list single_configurable_frame { $$ = $1; $$[$2.name] = $2; }
        ;
single_configurable_frame_list.opt // not in specification
        : %empty { $$ = std::map<std::string, ConfigurableFrame>(); }
        | single_configurable_frame_list { $$ = $1; }
        ;
message_id // message_id in specification
        : INTEGER { $$ = $1; }
        ;

    /* 9.2.2.3 Node composition definition */
node_composition_def // Node_composition_def in specification
        : COMPOSITE OPEN_BRACE
          node_configuration_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
node_configuration_def // not in specification
        : CONFIGURATION configuration_name OPEN_BRACE
          single_node_composition_def_list.opt
          CLOSE_BRACE { $$ = NodeConfiguration($2, $4); }
        ;
node_configuration_def_list // not in specification
        : node_configuration_def { $$ = std::map<std::string, NodeConfiguration>(); $$[$1.name] = $1; }
        | node_configuration_def_list node_configuration_def { $$ = $1; $$[$2.name] = $2; }
        ;
node_configuration_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, NodeConfiguration>(); }
        | node_configuration_def_list { $$ = $1; }
        ;
single_node_composition_def // not in specification
        : composite_name OPEN_BRACE
          logical_node_list SEMICOLON
          CLOSE_BRACE { $$ = NodeComposition($1, $3); }
        ;
single_node_composition_def_list // not in specification
        : single_node_composition_def { $$ = std::map<std::string, NodeComposition>(); $$[$1.name] = $1; }
        | single_node_composition_def_list single_node_composition_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_node_composition_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, NodeComposition>(); }
        | single_node_composition_def_list { $$ = $1; }
        ;
logical_node_list // not in specification
        : logical_node { $$ = std::vector<std::string>(); $$.push_back($1); }
        | logical_node_list COMMA logical_node { $$ = $1; $$.push_back($3); }
        ;
configuration_name // configuration_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
composite_name // composite_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
logical_node // logical_node in specification
        : IDENTIFIER { $$ = $1; }
        ;

    /* 9.2.3.1 Standard signals */
signal_def // Signal_def in specification
        : SIGNALS OPEN_BRACE
          single_signal_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_signal_def // not in specification
        : signal_name COLON signal_size COMMA init_value COMMA published_by subscribed_by_list.opt SEMICOLON { $$ = Signal($1, $3, $5, $7, $8); }
        ;
single_signal_def_list // not in specification
        : single_signal_def { $$ = std::map<std::string, Signal>(); $$[$1.name] = $1; }
        | single_signal_def_list single_signal_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_signal_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, Signal>(); }
        | single_signal_def_list { $$ = $1; }
        ;
signal_name // signal_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
signal_size // signal_size in specification
        : INTEGER { $$ = $1; }
        ;
init_value // init_value in specification
        : init_value_scalar { $$ = InitValue(1, $1); }
        | init_value_array {$$ = InitValue(2, $1); }
        ;
init_value_scalar // init_value_scalar in specification
        : INTEGER { $$ = $1; }
        ;
init_value_array // init_value_array in specification
        : OPEN_BRACE integer_list CLOSE_BRACE { $$ = $2; }
        ;
published_by // published_by in specification
        : IDENTIFIER { $$ = $1; }
        ;
subscribed_by // subscribed_by in specification
        : IDENTIFIER { $$ = $1; }
        ;
subscribed_by_list.opt // not in specification
        : %empty { $$ = std::vector<std::string>(); }
        | subscribed_by_list.opt COMMA subscribed_by { $$ = $1; $$.push_back($3); }
        ;

    /* 9.2.3.2 Diagnostic signals */
diagnostic_signal_def // Diag(nostic)_signal_def in specification
        : DIAGNOSTIC_SIGNALS OPEN_BRACE
          single_diagnostic_signal_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_diagnostic_signal_def // not in specification
        : signal_name COLON signal_size COMMA init_value SEMICOLON { $$ = DiagnosticSignal($1, $3, $5); }
        ;
single_diagnostic_signal_def_list // not in specification
        : single_diagnostic_signal_def { $$ = std::map<std::string, DiagnosticSignal>(); $$[$1.name] = $1; }
        | single_diagnostic_signal_def_list single_diagnostic_signal_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_diagnostic_signal_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, DiagnosticSignal>(); }
        | single_diagnostic_signal_def_list { $$ = $1; }
        ;

    /* 9.2.3.3 Signal groups */
signal_groups_def // Signal_groups_def in specification
        : SIGNAL_GROUPS OPEN_BRACE
          single_signal_group_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_signal_group_def // not in specification
        : signal_group_name COLON group_size OPEN_BRACE
          signal_name_group_offset_def_list.opt
          CLOSE_BRACE { $$ = SignalGroup($1, $3, $5); }
        ;
single_signal_group_def_list // not in specification
        : single_signal_group_def { $$ = std::map<std::string, SignalGroup>(); $$[$1.name] = $1; }
        | single_signal_group_def_list single_signal_group_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_signal_group_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, SignalGroup>(); }
        | single_signal_group_def_list { $$ = $1; }
        ;
signal_name_group_offset_def // not in specification
        : signal_name COMMA group_offset SEMICOLON { $$ = NameOffset($1, $3); }
        ;
signal_name_group_offset_def_list // not in specification
        : signal_name_group_offset_def { $$ = std::map<std::string, NameOffset>(); $$[$1.name] = $1; }
        | signal_name_group_offset_def_list signal_name_group_offset_def { $$ = $1; $$[$2.name] = $2; }
        ;
signal_name_group_offset_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, NameOffset>(); }
        | signal_name_group_offset_def_list { $$ = $1; }
        ;
signal_group_name // signal_group_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
group_size // group_size in specification
        : INTEGER { $$ = $1; }
        ;
group_offset // group_offset in specification
        : INTEGER { $$ = $1; }
        ;

    /* 9.2.4.1 Unconditional frames */
frame_def // Frame_def in specification
        : FRAMES OPEN_BRACE
          single_frame_def_list.opt
          CLOSE_BRACE { $$ = $3; }
          ;
single_frame_def // not in specification
        : frame_name COLON frame_id COMMA published_by COMMA frame_size OPEN_BRACE
          signal_name_offset_def_list.opt
          CLOSE_BRACE { $$ = Frame($1, $3, $5, $7, $9); }
        ;
single_frame_def_list // not in specification
        : single_frame_def { $$ = std::map<std::string, Frame>(); $$[$1.name] = $1; }
        | single_frame_def_list single_frame_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_frame_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, Frame>(); }
        | single_frame_def_list { $$ = $1; }
        ;
signal_name_offset_def // not in specification
        : signal_name COMMA signal_offset SEMICOLON { $$ = NameOffset($1, $3); }
        ;
signal_name_offset_def_list // not in specification
        : signal_name_offset_def { $$ = std::map<std::string, NameOffset>(); $$[$1.name] = $1; }
        | signal_name_offset_def_list signal_name_offset_def { $$ = $1; $$[$2.name] = $2; }
        ;
signal_name_offset_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, NameOffset>(); }
        | signal_name_offset_def_list { $$ = $1; }
        ;
frame_name // frame_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
frame_name.opt // not in specification
        : %empty { $$ = std::string(); }
        | COMMA frame_name { $$ = $2; }
        ;
frame_id // frame_id in specification
        : INTEGER { $$ = $1; }
        ;
frame_size // frame_size in specification
        : INTEGER { $$ = $1; }
        ;
signal_offset // signal_offset in specification
        : INTEGER { $$ = $1; }
        ;

    /* 9.2.4.2 Sporadic frames */
sporadic_frame_def // Sporadic_frame_def in specification
        : SPORADIC_FRAMES OPEN_BRACE
          single_sporadic_frame_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_sporadic_frame_def // not in specification
        : sporadic_frame_name COLON frame_name_list_comma SEMICOLON { $$ = SporadicFrame($1, $3); }
        ;
frame_name_list_comma // not in specification
        : frame_name { $$ = std::vector<std::string>(); $$.push_back($1); }
        | frame_name_list_comma COMMA frame_name { $$ = $1; $$.push_back($3); }
        ;
frame_name_list_comma.opt // not in specification
        : %empty { $$ = std::vector<std::string>(); }
        | frame_name_list_comma.opt COMMA frame_name { $$ = $1; $$.push_back($3); }
        ;
single_sporadic_frame_def_list // not in specification
        : single_sporadic_frame_def { $$ = std::map<std::string, SporadicFrame>(); $$[$1.name] = $1; }
        | single_sporadic_frame_def_list single_sporadic_frame_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_sporadic_frame_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, SporadicFrame>(); }
        | single_sporadic_frame_def_list { $$ = $1; }
        ;
sporadic_frame_name // sporadic_frame_name in specification
        : IDENTIFIER { $$ = $1; }
        ;

    /* 9.2.4.3 Event triggered frames */
event_triggered_frame_def // Event_triggered_frame_def in specification
        : EVENT_TRIGGERED_FRAMES OPEN_BRACE
          single_event_triggered_frame_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_event_triggered_frame_def // not in specification
        : event_trig_frm_name COLON
          collision_resolving_schedule_table COMMA
          frame_id
          frame_name_list_comma.opt SEMICOLON { $$ = EventTriggeredFrame($1, $3, $5, $6); }
        ;
single_event_triggered_frame_def_list // not in specification
        : single_event_triggered_frame_def { $$ = std::map<std::string, EventTriggeredFrame>(); $$[$1.name] = $1; }
        | single_event_triggered_frame_def_list single_event_triggered_frame_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_event_triggered_frame_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, EventTriggeredFrame>(); }
        | single_event_triggered_frame_def_list { $$ = $1; }
        ;
event_trig_frm_name // event_trig_frm_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
collision_resolving_schedule_table // collision_resolving_schedule_table in specification
        : IDENTIFIER { $$ = $1; }
        ;

    /* 9.2.4.4 Diagnostic frames */
diag_frame_def // Diag_frame_def in specification
        : DIAGNOSTIC_FRAMES OPEN_BRACE
          MASTERREQ COLON INTEGER OPEN_BRACE
          single_diag_frame_def_list.opt
          CLOSE_BRACE
          SLAVERESP COLON INTEGER OPEN_BRACE
          single_diag_frame_def_list.opt
          CLOSE_BRACE
          CLOSE_BRACE { $$ = DiagnosticFrames($5, $7, $11, $13); }
        ;
single_diag_frame_def // not in specification
        : frame_name COMMA INTEGER SEMICOLON { $$ = DiagnosticFrame($1, $3); }
        ;
single_diag_frame_def_list // not in specification
        : single_diag_frame_def { $$ = std::map<std::string, DiagnosticFrame>(); $$[$1.name] = $1; }
        | single_diag_frame_def_list single_diag_frame_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_diag_frame_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, DiagnosticFrame>(); }
        | single_diag_frame_def_list { $$ = $1; }
        ;

    /* 9.2.5 Schedule table definition */
schedule_table_def // Schedule_table_def in specification
        : SCHEDULE_TABLES OPEN_BRACE
          single_schedule_table_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_schedule_table_def // not in specification
        : schedule_table_name OPEN_BRACE
          single_command_def_list.opt
          CLOSE_BRACE { $$ = ScheduleTable($1, $3); }
        ;
single_schedule_table_def_list // not in specification
        : single_schedule_table_def { $$ = std::map<std::string, ScheduleTable>(); $$[$1.name] = $1; }
        | single_schedule_table_def_list single_schedule_table_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_schedule_table_def_list.opt  // not in specification
        : %empty { $$ = std::map<std::string, ScheduleTable>(); }
        | single_schedule_table_def_list { $$ = $1; }
        ;
single_command_def // not in specification
        : command DELAY frame_time MS SEMICOLON { $$ = CommandTime($1, $3); }
        ;
single_command_def_list // not in specification
        : single_command_def { $$ = std::vector<CommandTime>(); $$.push_back($1); }
        | single_command_def_list single_command_def { $$ = $1; $$.push_back($2); }
        ;
single_command_def_list.opt // not in specification
        : %empty { $$ = std::vector<CommandTime>(); }
        | single_command_def_list { $$ = $1; }
        ;
schedule_table_name // schedule_table_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
command // command in specification
        : frame_name { $$ = Command(1, $1); }
        | MASTERREQ { $$ = Command(2); }
        | SLAVERESP { $$ = Command(3); }
        | ASSIGNNAD OPEN_BRACE node_name CLOSE_BRACE { $$ = Command(4, $3); }
        | CONDITIONALCHANGENAD OPEN_BRACE INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER CLOSE_BRACE { $$ = Command(5, $3, $5, $7, $9, $11, $13); }
        | DATADUMP OPEN_BRACE node_name COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER CLOSE_BRACE { $$ = Command(6, $3, {$5, $7, $9, $11, $13}); }
        | SAVECONFIGURATION OPEN_BRACE node_name CLOSE_BRACE { $$ = Command(7, $3); }
        | ASSIGNFRAMEIDRANGE OPEN_BRACE node_name COMMA frame_index frame_pids.opt CLOSE_BRACE { $$ = Command(8, $3, $5, $6); }
        | FREEFORMAT OPEN_BRACE INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER COMMA INTEGER CLOSE_BRACE { $$ = Command(9, {$3, $5, $7, $9, $11, $13, $15, $17}); }
        | ASSIGNFRAMEID OPEN_BRACE node_name COMMA frame_name CLOSE_BRACE { $$ = Command(10, $3, $5); }
        ;
frame_pids // not in specification
        : COMMA frame_pid COMMA frame_pid COMMA frame_pid COMMA frame_pid { $$ = {$2, $4, $6, $8}; }
        ;
frame_pids.opt // not in specification
        : %empty { $$ = std::vector<std::string>(); }
        | frame_pids { $$ = $1; }
        ;
frame_index // frame_index in specification
        : INTEGER { $$ = $1; }
        ;
frame_pid // frame_pid in specification
        : INTEGER { $$ = $1; }
        ;
frame_time // frame_time in specification
        : real_or_integer { $$ = $1; }
        ;

    /* 9.2.6.1 Signal encoding type definition */
signal_encoding_type_def // Signal_encoding_type_def in specification
        : SIGNAL_ENCODING_TYPES OPEN_BRACE
          single_signal_encoding_type_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_signal_encoding_type_def // not in specification
        : signal_encoding_type_name OPEN_BRACE
          signal_encoding_value_list.opt
          CLOSE_BRACE { $$ = Encoding($1, $3); }
        ;
single_signal_encoding_type_def_list // not in specification
        : single_signal_encoding_type_def { $$ = std::map<std::string, Encoding>(); $$[$1.name] = $1; }
        | single_signal_encoding_type_def_list single_signal_encoding_type_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_signal_encoding_type_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, Encoding>(); }
        | single_signal_encoding_type_def_list { $$ = $1; }
        ;
signal_encoding_value // not in specification
        : logical_value { $$ = EncodingValue(1, $1); }
        | physical_range { $$ = EncodingValue(2, $1); }
        | bcd_value { $$ = EncodingValue(3); }
        | ascii_value { $$ = EncodingValue(4); }
        ;
signal_encoding_value_list // not in specification
        : signal_encoding_value { $$ = std::vector<EncodingValue>(); $$.push_back($1); }
        | signal_encoding_value_list signal_encoding_value { $$ = $1; $$.push_back($2); }
        ;
signal_encoding_value_list.opt // not in specification
        : %empty { $$ = std::vector<EncodingValue>(); }
        | signal_encoding_value_list { $$ = $1; }
        ;
signal_encoding_type_name // signal_encoding_type_name in specification
        : IDENTIFIER { $$ = $1; }
        ;
logical_value // logical_value in specification
        : LOGICAL_VALUE COMMA signal_value SEMICOLON { $$ = LogicalValue($3); }
        | LOGICAL_VALUE COMMA signal_value COMMA text_info SEMICOLON { $$ = LogicalValue($3, $5); }
        ;
physical_range // physical_range in specification
        : PHYSICAL_VALUE COMMA min_value COMMA max_value COMMA scale COMMA offset SEMICOLON { $$ = PhysicalRange($3, $5, $7, $9); }
        | PHYSICAL_VALUE COMMA min_value COMMA max_value COMMA scale COMMA offset COMMA text_info SEMICOLON { $$ = PhysicalRange($3, $5, $7, $9, $11); }
        ;
bcd_value // bcd_value in specification
        : BCD_VALUE SEMICOLON
        ;
ascii_value // ascii_value in specification
        : ASCII_VALUE SEMICOLON
        ;
signal_value // signal_value in specification
        : INTEGER { $$ = $1; }
        ;
min_value // min_value in specification
        : INTEGER { $$ = $1; }
        ;
max_value // max_value in specification
        : INTEGER { $$ = $1; }
        ;
scale // scale in specification
        : real_or_integer { $$ = $1; }
        ;
offset // offset in specification
        : real_or_integer { $$ = $1; }
        ;
text_info // text_info in specification
        : CHAR_STRING { $$ = $1; }
        ;

    /* 9.2.6.2 Signal representation definition */
signal_representation_def // Signal_representation_def in specification
        : SIGNAL_REPRESENTATION OPEN_BRACE
          single_signal_representation_def_list.opt
          CLOSE_BRACE { $$ = $3; }
        ;
single_signal_representation_def // not in specification
        : signal_encoding_type_name COLON signal_name_list.opt SEMICOLON { $$ = SignalRepresentation($1, $3); }
        ;
single_signal_representation_def_list // not in specification
        : single_signal_representation_def { $$ = std::map<std::string, SignalRepresentation>(); $$[$1.name] = $1; }
        | single_signal_representation_def_list single_signal_representation_def { $$ = $1; $$[$2.name] = $2; }
        ;
single_signal_representation_def_list.opt // not in specification
        : %empty { $$ = std::map<std::string, SignalRepresentation>(); }
        | single_signal_representation_def_list { $$ = $1; }
        ;
signal_name_list // not in specification
        : signal_name { $$ = std::vector<std::string>(); $$.push_back($1); }
        | signal_name_list COMMA signal_name { $$ = $1; $$.push_back($3); }
        ;
signal_name_list.opt // not in specification
        : %empty { $$ = std::vector<std::string>(); }
        | signal_name_list { $$ = $1; }
        ;

    /* 9.3 Overview of syntax */
integer_list // not in specification
        : INTEGER { $$ = std::vector<std::string>(); $$.push_back($1); }
        | integer_list COMMA INTEGER { $$ = $1; $$.push_back($3); }
        ;
real_or_integer // real_or_integer in specification
        : REAL { $$ = RealOrInteger(1, $1); }
        | INTEGER { $$ = RealOrInteger(2, $1); }
        ;

%%

void LIN::LDF::Parser::error(const location_type & location, const std::string & message)
{
    std::cerr << "Parse error at " << location << ": " << message << std::endl;
}
