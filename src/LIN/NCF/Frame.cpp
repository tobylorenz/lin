/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/Frame.h>

namespace LIN {
namespace NCF {

Frame::Frame() :
    kind(),
    name(),
    properties(),
    signals()
{
}

Frame::Frame(uint8_t kind, std::string & name, std::vector<FrameProperty> & properties, std::map<std::string, Signal> & signals) :
    kind(kind),
    name(name),
    properties(properties),
    signals(signals)
{
}

std::ostream & operator<<(std::ostream & os, Frame & obj)
{
    os << "    ";
    switch (obj.kind) {
    case 1:
        os << "publish";
        break;
    case 2:
        os << "subscribe";
        break;
    }
    os << " " << obj.name << " {" << std::endl;

    for (FrameProperty & property : obj.properties) {
        os << property;
    }

    os << "      signals {" << std::endl;

    for (std::pair<std::string, Signal> signal : obj.signals) {
        os << signal.second;
    }

    os << "      }" << std::endl;

    os << "    }" << std::endl;

    return os;
}

}
}
