/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/DiagnosticProperty.h>

namespace LIN {
namespace NCF {

DiagnosticProperty::DiagnosticProperty() :
    type(0),
    nad(),
    str(),
    duration(),
    supportSids()
{
}

DiagnosticProperty::DiagnosticProperty(uint8_t type, NodeAddress & nad) :
    type(type),
    nad(nad),
    str(),
    duration(),
    supportSids()
{
}

DiagnosticProperty::DiagnosticProperty(uint8_t type, std::string & str) :
    type(type),
    nad(),
    str(str),
    duration(),
    supportSids()
{
}

DiagnosticProperty::DiagnosticProperty(uint8_t type, RealOrInteger & duration) :
    type(type),
    nad(),
    str(),
    duration(duration),
    supportSids()
{
}

DiagnosticProperty::DiagnosticProperty(uint8_t type, std::vector<std::string> & supportSids) :
    type(type),
    nad(),
    str(),
    duration(),
    supportSids(supportSids)
{
}

std::ostream & operator<<(std::ostream & os, DiagnosticProperty & obj)
{
    switch (obj.type) {
    case 1:
        os << "    NAD = " << obj.nad << ";" << std::endl;
        break;
    case 2:
        os << "    diagnostic_class = " << obj.str << ";" << std::endl;
        break;
    case 3:
        os << "    P2_min = " << obj.duration << " ms;" << std::endl;
        break;
    case 4:
        os << "    ST_min = " << obj.duration << " ms;" << std::endl;
        break;
    case 5:
        os << "    N_As_timeout = " << obj.duration << " ms;" << std::endl;
        break;
    case 6:
        os << "    N_Cr_timeout = " << obj.duration << " ms;" << std::endl;
        break;
    case 7: {
        os << "    support_sid { ";
        bool first = true;
        for (std::string & supportSid : obj.supportSids) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << supportSid;
        }
        os << " };" << std::endl;
    }
        break;
    case 8:
        os << "    max_message_length = " << obj.str << ";" << std::endl;
        break;
    }

    return os;
}

}
}
