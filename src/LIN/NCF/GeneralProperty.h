/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <cstdint>
#include <ostream>
#include <string>

#include <LIN/NCF/Bitrates.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** general property (general_definition) */
class LIN_NCF_EXPORT GeneralProperty
{
public:
    GeneralProperty();
    GeneralProperty(uint8_t type, std::string & str);
    GeneralProperty(uint8_t type, Bitrates & bitrate);
    GeneralProperty(uint8_t type, bool sendsWakeUpSignal);

    // 0 = undefined
    // 1 = LIN_protocol_version
    // 2 = supplier
    // 3 = function
    // 4 = variant
    // 5 = bitrate
    // 6 = sends_wake_up_signal
    uint8_t type;

    /**
     * protocol version (LIN_protocol_version, protocol_version)
     * supplier (supplier, supplier_id)
     * function (function, function_id)
     * variant (variant, variant_id)
     */
    std::string str;

    /** bitrate (bitrate) */
    Bitrates bitrate;

    /** sends wake up signal (sends_wake_up_signal) */
    bool sendsWakeUpSignal;
};

std::ostream & operator<<(std::ostream & os, GeneralProperty & obj);

}
}
