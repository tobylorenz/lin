/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/FrameProperty.h>

namespace LIN {
namespace NCF {

FrameProperty::FrameProperty() :
    type(0),
    str()
{
}

FrameProperty::FrameProperty(uint8_t type, std::string & str) :
    type(type),
    str(str)
{
}

std::ostream & operator<<(std::ostream & os, FrameProperty & obj)
{
    switch (obj.type) {
    case 1:
        os << "      length = " << obj.str << ";" << std::endl;
        break;
    case 2:
        os << "      min_period = " << obj.str << " ms;" << std::endl;
        break;
    case 3:
        os << "      max_period = " << obj.str << " ms;" << std::endl;
        break;
    case 4:
        os << "      event_triggered_frame = " << obj.str << ";" << std::endl;
        break;
    }

    return os;
}

}
}
