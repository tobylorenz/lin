/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <iostream>
#include <map>
#include <string>

#include <LIN/NCF/Node.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** Node Capability File (node_capability_file) */
class LIN_NCF_EXPORT NodeCapabilityFile
{
public:
    NodeCapabilityFile();

    /** language Version (LIN_language_version) */
    std::string languageVersion;

    /** nodes */
    std::map<std::string, Node> nodes;

    /** successfully parsed */
    bool successfullyParsed;
};

LIN_NCF_EXPORT std::ostream & operator<<(std::ostream & os, NodeCapabilityFile & obj);
LIN_NCF_EXPORT std::istream & operator>>(std::istream & is, NodeCapabilityFile & obj);

}
}
