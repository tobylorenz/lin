/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/EncodingValue.h>

namespace LIN {
namespace NCF {

EncodingValue::EncodingValue() :
    type(0),
    logicalValue(),
    physicalRange()
{
}

EncodingValue::EncodingValue(uint8_t type, LogicalValue & logicalValue) :
    type(type),
    logicalValue(logicalValue),
    physicalRange()
{
}

EncodingValue::EncodingValue(uint8_t type, PhysicalRange & physicalRange) :
    type(type),
    logicalValue(),
    physicalRange(physicalRange)
{
}

EncodingValue::EncodingValue(uint8_t type) :
    type(type),
    logicalValue(),
    physicalRange()
{
}

std::ostream & operator<<(std::ostream & os, EncodingValue & obj)
{
    switch (obj.type) {
    case 1:
        os << obj.logicalValue;
        break;
    case 2:
        os << obj.physicalRange;
        break;
    case 3:
        os << "      bcd_value;" << std::endl;
        break;
    case 4:
        os << "      ascii_value;" << std::endl;
        break;
    }

    return os;
}

}
}
