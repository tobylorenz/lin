/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <map>
#include <ostream>
#include <string>
#include <vector>

#include <LIN/NCF/FrameProperty.h>
#include <LIN/NCF/Signal.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** frame (single_frame) */
class LIN_NCF_EXPORT Frame
{
public:
    Frame();
    Frame(uint8_t kind, std::string & name, std::vector<FrameProperty> & properties, std::map<std::string, Signal> & signals);

    /**
     * kind (frame_kind)
     *
     * 1 = publish
     * 2 = subscribe
     */
    uint8_t kind;

    /** name (frame_name, identifier) */
    std::string name;

    /** properties (frame_properies) */
    std::vector<FrameProperty> properties;

    /** signals (signal_definition) */
    std::map<std::string, Signal> signals;
};

std::ostream & operator<<(std::ostream & os, Frame & obj);

}
}
