/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <cstdint>
#include <ostream>
#include <vector>

#include <LIN/NCF/RealOrInteger.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** bit rates (bitrate_definition) */
class LIN_NCF_EXPORT Bitrates
{
public:
    Bitrates();
    Bitrates(uint8_t type, RealOrInteger & automaticMin, RealOrInteger & automaticMax); // automatic
    Bitrates(uint8_t type, std::vector<RealOrInteger> & selectList); // select
    Bitrates(uint8_t type, RealOrInteger & fixed); // fixed

    // 0 = undefined
    // 1 = automatic (2 bitrates: min, max)
    // 2 = select (n bitrates)
    // 3 = fixed (1 bitrate)
    uint8_t type;

    /** bitrates (bitrate) */
    std::vector<RealOrInteger> bitrates;
};

std::ostream & operator<<(std::ostream & os, Bitrates & obj);

}
}
