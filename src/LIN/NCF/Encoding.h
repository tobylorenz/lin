/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <ostream>
#include <string>
#include <vector>

#include <LIN/NCF/EncodingValue.h>

#include <LIN/NCF/lin_ncf_export.h>

namespace LIN {
namespace NCF {

/** encoding (encoding_definition) */
class LIN_NCF_EXPORT Encoding
{
public:
    Encoding();
    Encoding(std::string & name, std::vector<EncodingValue> & values);

    /** name (encoding_name) */
    std::string name;

    /** values (logical_value, physical_range, bcd_value, ascii_value) */
    std::vector<EncodingValue> values;
};

std::ostream & operator<<(std::ostream & os, Encoding & obj);

}
}
