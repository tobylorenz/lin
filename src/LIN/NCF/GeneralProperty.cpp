/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/GeneralProperty.h>

namespace LIN {
namespace NCF {

GeneralProperty::GeneralProperty() :
    type(0),
    str(),
    bitrate(),
    sendsWakeUpSignal(false)
{
}

GeneralProperty::GeneralProperty(uint8_t type, std::string & str) :
    type(type),
    str(str),
    bitrate(),
    sendsWakeUpSignal(false)
{
}

GeneralProperty::GeneralProperty(uint8_t type, Bitrates & bitrate) :
    type(type),
    str(),
    bitrate(bitrate),
    sendsWakeUpSignal(false)
{
}

GeneralProperty::GeneralProperty(uint8_t type, bool sendsWakeUpSignal) :
    type(type),
    str(),
    bitrate(),
    sendsWakeUpSignal(sendsWakeUpSignal)
{
}

std::ostream & operator<<(std::ostream & os, GeneralProperty & obj)
{
    switch (obj.type) {
    case 1:
        os << "    LIN_protocol_version = " << obj.str << ";" << std::endl;
        break;
    case 2:
        os << "    supplier = " << obj.str << ";" << std::endl;
        break;
    case 3:
        os << "    function = " << obj.str << ";" << std::endl;
        break;
    case 4:
        os << "    variant = " << obj.str << ";" << std::endl;
        break;
    case 5:
        os << "    bitrate = " << obj.bitrate << ";" << std::endl;
        break;
    case 6:
        os << "    sends_wake_up_signal = \"" << (obj.sendsWakeUpSignal ? "yes" : "no") << "\";" << std::endl;
        break;
    }

    return os;
}

}
}
