/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <LIN/NCF/platform.h>

#include <iostream>

#if !defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#define YY_DECL LIN::NCF::Parser::symbol_type LIN::NCF::Scanner::yylex(const LIN::NCF::Parser::location_type & loc)

#define YY_USER_ACTION \
    location.begin.line = location.end.line; \
    location.begin.column = location.end.column; \
    for(int i = 0; yytext[i] != '\0'; i++) { \
        if(yytext[i] == '\n') { \
            location.end.line++; \
            location.end.column = 0; \
        } \
        else { \
            location.end.column++; \
        } \
    }

#include <LIN/NCF/Parser.hpp>

namespace LIN {
namespace NCF {

/**
 * @brief Lex scanner class
 *
 * This contains the converter-specific Flex Lexer class.
 */
class Scanner : public yyFlexLexer
{
public:
    Scanner(std::istream & istream) :
        yyFlexLexer(&istream),
        location()
    { }

    /**
     * lexer function
     *
     * @param loc location
     * @return symbol type
     */
    Parser::symbol_type yylex(const Parser::location_type & loc);

    /** location */
    Parser::location_type location;
};

}
}
