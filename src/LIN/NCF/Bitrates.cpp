/*
 * Copyright (C) 2018-2019 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <LIN/NCF/Bitrates.h>

namespace LIN {
namespace NCF {

Bitrates::Bitrates() :
    type(0),
    bitrates()
{
}

Bitrates::Bitrates(uint8_t type, RealOrInteger & automaticMin, RealOrInteger & automaticMax) :
    type(type),
    bitrates({automaticMin, automaticMax})
{
}

Bitrates::Bitrates(uint8_t type, std::vector<RealOrInteger> & selectList) :
    type(type),
    bitrates(selectList)
{
}

Bitrates::Bitrates(uint8_t type, RealOrInteger & fixed) :
    type(type),
    bitrates({fixed})
{
}

std::ostream & operator<<(std::ostream & os, Bitrates & obj)
{
    switch (obj.type) {
    case 1:
        os << "automatic";
        if (obj.bitrates[0].type) {
            os << " min " << obj.bitrates[0] << " kbps";
        }
        if (obj.bitrates[1].type) {
            os << " max " << obj.bitrates[1] << " kbps";
        }
        break;
    case 2: {
        os << "select { ";
        bool first = true;
        for (RealOrInteger & bitrate : obj.bitrates) {
            if (!first) {
                os << ", ";
            }
            first = false;
            os << bitrate << " kbps";
        }
        os << " }";
    }
        break;
    case 3:
        os << obj.bitrates[0] << " kbps";
        break;
    }

    return os;
}

}
}
